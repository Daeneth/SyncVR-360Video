﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ButtonControl : MonoBehaviour
{
    public void BackToMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void VideoSelector()
    {
        SceneManager.LoadScene("Video Selection");
    }

    public void PlayLionVideo()
    {
        SceneManager.LoadScene("Lion Video");
    }

    public void PlayLondonVideo()
    {
        SceneManager.LoadScene("London Video");
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
